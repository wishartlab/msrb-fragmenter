package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Pattern;

import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.exception.InvalidSmilesException;
import org.openscience.cdk.inchi.InChIGenerator;
import org.openscience.cdk.inchi.InChIGeneratorFactory;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IChemObjectBuilder;
import org.openscience.cdk.interfaces.IMolecularFormula;
import org.openscience.cdk.silent.SilentChemObjectBuilder;
import org.openscience.cdk.smiles.SmilesParser;
import org.openscience.cdk.tools.manipulator.MolecularFormulaManipulator;

import wishartlab.cfmid_plus.fragmentation.FPLists;
import wishartlab.cfmid_plus.fragmentation.FragmentationCondition;
import wishartlab.cfmid_plus.fragmentation.FragmentationPattern;
import wishartlab.cfmid_plus.fragmentation.Fragmenter;
import wishartlab.cfmid_plus.molecules.StructuralClass;
import wishartlab.cfmid_plus.molecules.StructuralClass.ClassName;
import wishartlab.cfmid_plus.molecules.StructureExplorer;

public class framentForInputFile {
	IChemObjectBuilder 	builder = SilentChemObjectBuilder.getInstance();
	SmilesParser	smiParser		= new SmilesParser(builder);
    IChemObjectBuilder bldr = DefaultChemObjectBuilder.getInstance();
    Fragmenter fr = new Fragmenter();	
    String smiles;
    String inChIKey;
    String id;
    StructureExplorer sExplorer = new StructureExplorer();
    boolean print = true;
    public void generateResults(String inputPath, String type) throws Exception{
    	File inputFile = new File(inputPath);
    	String iname = inputFile.getName();   	
    	BufferedReader br = new BufferedReader(new FileReader(inputFile));
    	String oneLine;
    	InChIGeneratorFactory factory = InChIGeneratorFactory.getInstance();
    	while((oneLine = br.readLine()) != null){
    		if(oneLine.isEmpty()) break;
    		String[] parseLine = oneLine.split("\t");
    		if(parseLine.length < 2) parseLine = oneLine.split(" ");
    		if(parseLine.length < 2) throw new Exception("You should seperate the input InChIKey and SMILES String by either space or tab");
    		String id = parseLine[0];
    		String smiles = parseLine[1];
    		IAtomContainer singleInput = smiParser.parseSmiles(smiles.replace("[O-]", "O"));
    		InChIGenerator gen = factory.getInChIGenerator(singleInput);
    		String inChiKey = gen.getInchiKey();
    		fr.SMILES = smiles;
    		this.smiles = smiles;
    		this.inChIKey = inChiKey;
    		this.id = id;
    		fr.InChiKey = inChIKey;
    		
    		if(type.equalsIgnoreCase("whole")){
    			String outputPath = iname.replace(".txt", "log");
    			saveSingleCfmidLikeMSPeakList(singleInput, bldr, outputPath);
    		}
    		else if(type.equalsIgnoreCase("individual")){
    			//String outputPath = iname.replace(".txt", "log");
    			fr.saveSingleCfmidLikeMSPeakList(singleInput, bldr, inChIKey);
    		}
    		else{
    			br.close();
    			throw new Exception("The user must specify the output type as either 'whole' or 'indivudual' when using this 'ifile' parameter");
    		}
    	}
    	br.close();
    	System.out.println("job done");
    }
    
	public void saveSingleCfmidLikeMSPeakList(IAtomContainer molecule, IChemObjectBuilder bldr, String outputname) throws Exception{
		IAtomContainer standardized_mol = this.sExplorer.standardizeMolecule(molecule);
		StructuralClass.ClassName type = StructureExplorer.findClassName(standardized_mol);
		saveSingleCfmidLikeMSPeakList(standardized_mol, bldr, type, outputname, false);
	
	}
	public void saveSingleCfmidLikeMSPeakList(IAtomContainer molecule, IChemObjectBuilder bldr, StructuralClass.ClassName type, String outputname, boolean standardize) throws Exception{
		
		IAtomContainer standardized_mol = molecule;
		if(standardize){
			standardized_mol = this.sExplorer.standardizeMolecule(molecule);
		}

		if(FPLists.classSpecificFragmentationPatterns.containsKey(type)){
			//System.out.println("\nSTATUS REPORT = 0\nPredicting spectra for all covered adduct types.");
			for(String adduct : FPLists.classSpecificFragmentationPatterns.get(type).keySet()){
				LinkedHashMap<String, FragmentationPattern> check = FPLists.classSpecificFragmentationPatterns.get(type);
				//System.out.println("Generating peak list for the adduct type: " + String.valueOf(adduct));
				LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<String>>> annotatedPeaks = new LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<String>>>();
				LinkedHashMap<String, IAtomContainer> fragments = this.fr.fragmentMolecule(standardized_mol, type, adduct);
				FragmentationCondition fragCondition_10 =  new FragmentationCondition(adduct, 10);
				FragmentationCondition fragCondition_20 =  new FragmentationCondition(adduct, 20);
				FragmentationCondition fragCondition_40 =  new FragmentationCondition(adduct, 40);
				
				annotatedPeaks.put(10, this.fr.annotatePeakList(fragments ,type, fragCondition_10));
				annotatedPeaks.put(20,	this.fr.annotatePeakList(fragments ,type, fragCondition_20));
				annotatedPeaks.put(40, this.fr.annotatePeakList(fragments ,type, fragCondition_40));
				if(annotatedPeaks==null || annotatedPeaks.isEmpty() || !isValidAnnotatedPeaksList(annotatedPeaks)) {
					System.out.println("STATUS REPORT = 7\\nNo results produced by msrb.");
					return;
				}
				if(!print){
					File outfile = new File(outputname);
					String name = outfile.getName();
					String parentDir = outfile.getParent();
					
					
					String[] nameSplit = name.split(Pattern.quote("."));
					String fullname = null;
					if(nameSplit.length > 1){
						if (parentDir != null){
							//fullname = parentDir + "/" + nameSplit[0] + "_" + adduct + "." + nameSplit[1];
							fullname = parentDir + "/" + name + "_" + adduct + ".log";
						}
						else {
							//fullname = nameSplit[0] + "_" + adduct + "." + nameSplit[1];
							fullname = name + "_" + adduct + ".log";
						}
						
					}
					else if(nameSplit.length == 1){
						if (parentDir != null){
							//fullname = parentDir + "/" + nameSplit[0] + "_" + adduct;
							fullname = parentDir + "/" + name + "_" + adduct + ".log";
						}
						else {
							//fullname = nameSplit[0] + "_" + adduct;
							fullname = nameSplit[0] + "_" + adduct + ".log";
						}
						
					}			
					saveSingleCfmidLikeMSAnnotatedPeakList(annotatedPeaks, adduct, fullname);
				}
				else printSingleCfmidLikeMSAnnotatedPeakList(annotatedPeaks, adduct);
				//System.out.println("Saving spectrum to " + fullname);
				
			}			
		}
		else{
        	if(type == ClassName.GLYCEROLIPIDS || type == ClassName.GLYCEROPHOSPHOLIPIDS || type == ClassName.SPHINGOLIPIDS 
       			|| type == ClassName.CERAMIDE_1_PHOSPHATES || type == ClassName.DIPHOSPHORYLATED_HEXAACYL_LIPID_A ||
        			type == ClassName.SULFATIDES || type == ClassName.FATTY_ACID_ESTERS_OF_HYDROXYL_FATTY_ACIDS ||
        			type == ClassName.ETHER_LIPIDS
        			){            	
    			System.out.println("STATUS REPORT = 4\nThe compound belongs to a lipid class of " + type + ", which is not covered "
    					+ "in the current version of the fragmenter.");       		
        	}
        	else{
    			System.out.println("STATUS REPORT = 5\nInvalid chemical class. The query compound does not belong to any of the classes covered "
    					+ "in the current version of the fragmenter.");         		
       	}
        }
	}
   
	public void saveSingleCfmidLikeMSAnnotatedPeakList(LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<String>>>annotatedPeaks, String adductType, String outputname) throws IOException, InvalidSmilesException{
		
		if(annotatedPeaks != null) {
			File checkFile = new File(outputname);
			if(!checkFile.exists()){
				checkFile.createNewFile();
		    }
			FileWriter fw = new FileWriter(outputname, true);
//			FileWriter fw = new FileWriter("data/" + outputname.replace("/", "_") + ".log");
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write("#In-silico ESI-MS/MS" + " " + adductType + "  " + "Spectra");	
			bw.newLine();
			System.out.println("#PREDICTED BY MSRB " + RuleBasedFrag.VERSION);
			System.out.println("#ID=" + this.id);
			System.out.println("#SMILES=" + this.smiles);
			System.out.println("#InChiKey="  + this.inChIKey);
			IAtomContainer tempMole = this.smiParser.parseSmiles(this.smiles);
			IMolecularFormula m = MolecularFormulaManipulator.getMolecularFormula(tempMole);	
			String formula = MolecularFormulaManipulator.getString(m);
			Double mass = MolecularFormulaManipulator.getTotalExactMass(m);
			Double pmass = getPmass(mass, adductType);
			System.out.println("#Formula=" + formula);
			if(pmass> 0.0){
				System.out.println("#PMass=" + pmass);
			}
			int level = 0;
			int fragIndex = 0;
			
			for(Map.Entry<Integer, LinkedHashMap<String, ArrayList<String>>> peaks : annotatedPeaks.entrySet()){
//				System.out.println("Energy: " + peaks.getKey());
//				System.out.println("peaks: " + peaks.getValue());
//				System.out.println("Nr. of peaks: " + peaks.getValue().get("peaks_list").size());
				bw.write("energy" + level);
				bw.newLine();
				level++;
				for(String s : peaks.getValue().get("peaks_list")){
//					System.out.println();
					bw.write(s);
					bw.newLine();
				}
			}
			
			bw.newLine();
			for( String fg : annotatedPeaks.get(10).get("fragments")){
				bw.write( fragIndex + " " + fg);
				bw.newLine();
				fragIndex++;
			}

			bw.newLine();
			bw.newLine();
			
			bw.close();	
			fw.close();
		}		
	}
	
	public void printSingleCfmidLikeMSAnnotatedPeakList(LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<String>>>annotatedPeaks, String adductType) throws IOException, InvalidSmilesException{
		
		if(annotatedPeaks != null) {
			System.out.println("#In-silico ESI-MS/MS" + " " + adductType + "  " + "Spectra");	
			//bw.newLine();
			System.out.println("#PREDICTED BY MSRB " + RuleBasedFrag.VERSION);
			System.out.println("#ID=" + this.id);
			System.out.println("#SMILES=" + this.smiles);
			System.out.println("#InChiKey="  + this.inChIKey);
			IAtomContainer tempMole = this.smiParser.parseSmiles(this.smiles);
			IMolecularFormula m = MolecularFormulaManipulator.getMolecularFormula(tempMole);	
			String formula = MolecularFormulaManipulator.getString(m);
			Double mass = MolecularFormulaManipulator.getTotalExactMass(m);
			Double pmass = getPmass(mass, adductType);
			System.out.println("#Formula=" + formula);
			if(pmass> 0.0){
				System.out.println("#PMass=" + pmass);
			}
			int level = 0;
			int fragIndex = 0;
			
			for(Map.Entry<Integer, LinkedHashMap<String, ArrayList<String>>> peaks : annotatedPeaks.entrySet()){
//				System.out.println("Energy: " + peaks.getKey());
//				System.out.println("peaks: " + peaks.getValue());
//				System.out.println("Nr. of peaks: " + peaks.getValue().get("peaks_list").size());
				System.out.println("energy" + level);
				level++;
				for(String s : peaks.getValue().get("peaks_list")){
//					System.out.println();
					System.out.println(s);
				}
			}
			
			System.out.println();
			for( String fg : annotatedPeaks.get(10).get("fragments")){
				System.out.println(fragIndex + " " + fg);
				
				fragIndex++;
			}

			System.out.println();
			System.out.println();
		}		
	}
	
	public Double getPmass(Double mass, String adductType){
		if(adductType.contains("[M+H]+")) return (mass + 1.007276);
		else if(adductType.contains("[M-H]-")) return (mass - 1.007276);
		else if(adductType.contains("[M+]"))	return (mass - 0.00054858);
		else if(adductType.contains("[M+Li]+")) return (mass + 6.9400376247955);
		else if(adductType.contains("[M+NH4]+")) return (mass + 18.033823);
		else if(adductType.contains("[M+Na]+")) return (mass + 22.989218);
		else if(adductType.contains("[M+Cl]-")) return (mass + 34.969402);
		else if(adductType.contains("[M-2H](2H)-")) return 0.0;
		else return 0.0;
		
	}
	
	public boolean isValidAnnotatedPeaksList(LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<String>>> annotatedPeaks) {
		for(Integer energy : annotatedPeaks.keySet()) {
			LinkedHashMap<String, ArrayList<String>> peaksMap = annotatedPeaks.get(energy);
			if(peaksMap == null || peaksMap.isEmpty()) continue;
			for(String peak : peaksMap.keySet()) {
				ArrayList<String> fragmentList = peaksMap.get(peak);
				if(fragmentList != null && !fragmentList.isEmpty()) {
					return true;
				}
			}
		}
		return false;
	}
    
}
