package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;

import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IChemObjectBuilder;
import org.openscience.cdk.silent.SilentChemObjectBuilder;
import org.openscience.cdk.smiles.SmilesParser;

import wishartlab.cfmid_plus.fragmentation.FPLists;
import wishartlab.cfmid_plus.fragmentation.FragmentationCondition;
import wishartlab.cfmid_plus.fragmentation.FragmentationPattern;
import wishartlab.cfmid_plus.fragmentation.Fragmenter;
import wishartlab.cfmid_plus.molecules.StructuralClass;
import wishartlab.cfmid_plus.molecules.StructuralClass.ClassName;
import wishartlab.cfmid_plus.molecules.StructureExplorer;


public class FragmentPrint {
	IChemObjectBuilder 	builder = SilentChemObjectBuilder.getInstance();
	SmilesParser	smiParser		= new SmilesParser(builder);
    IChemObjectBuilder bldr = DefaultChemObjectBuilder.getInstance();
    Fragmenter fr = new Fragmenter();	
    StructureExplorer sExplorer = new StructureExplorer();
    boolean print = true;
    framentForInputFile ffif = new framentForInputFile();
    public void generateResults(String inputPath, ArrayList<String> adduct_types, String type) throws Exception{
    	File inputFile = new File(inputPath);
    	String iname = inputFile.getName();   	
    	BufferedReader br = new BufferedReader(new FileReader(inputFile));
    	String oneLine;
    	while((oneLine = br.readLine()) != null){
    		if(oneLine.isEmpty()) break;
    		String[] parseLine = oneLine.split("\t");
    		if(parseLine.length < 2) parseLine = oneLine.split(" ");
    		if(parseLine.length < 2) throw new Exception("You should seperate the input InChIKey and SMILES String by either space or tab");
    		String inChIKey = parseLine[0];
    		String smiles = parseLine[1];
    		this.fr.SMILES = smiles;
    		this.fr.InChiKey = inChIKey;
    		this.ffif.smiles = smiles;
    		this.ffif.inChIKey = inChIKey;
    		IAtomContainer singleInput = smiParser.parseSmiles(smiles.replace("[O-]", "O"));
    		if(type.equalsIgnoreCase("whole")){   			
    			saveSingleCfmidLikeMSPeakList(singleInput, bldr, adduct_types, true);
    		}
    		else{
    			br.close();
    			throw new Exception("The user must specify the output type as either 'whole' or 'indivudual' when using this 'ifile' parameter");
    		}
    	}
    	br.close();
    	System.out.println("job done");
    }
	public void saveSingleCfmidLikeMSPeakList(IAtomContainer molecule, IChemObjectBuilder bldr, ArrayList<String> adduct_types, boolean noExtraAdduct) throws Exception{	
		IAtomContainer standardized_mol = this.sExplorer.standardizeMolecule(molecule);
		StructuralClass.ClassName type = StructureExplorer.findClassName(standardized_mol);
//		System.out.println("The type of this molecule is " + String.valueOf(type));

        if(FPLists.classSpecificFragmentationPatterns.containsKey(type)){
            saveSingleCfmidLikeMSPeakList(standardized_mol, bldr, type, adduct_types, noExtraAdduct);  	
        } 
        else{
        	
        	if(type == ClassName.GLYCEROLIPIDS || type == ClassName.GLYCEROPHOSPHOLIPIDS || type == ClassName.SPHINGOLIPIDS 
        			|| type == ClassName.CERAMIDE_1_PHOSPHATES || type == ClassName.DIPHOSPHORYLATED_HEXAACYL_LIPID_A ||
        			type == ClassName.SULFATIDES || type == ClassName.FATTY_ACID_ESTERS_OF_HYDROXYL_FATTY_ACIDS ||
        			type == ClassName.ETHER_LIPIDS
        			){            	
    			System.out.println("STATUS REPORT = 4\nThe compound belongs to the lipid class of " + type + ", which is not covered "
    					+ "in the current version of the fragmenter.");       		
        	}
        	else{
    			System.out.println("STATUS REPORT = 5\nInvalid chemical class. The query compound does not belong to any of the classes covered "
    					+ "in the current version of the fragmenter.");         		
        	}

        }
	}
	
	public void saveSingleCfmidLikeMSPeakList(IAtomContainer molecule, IChemObjectBuilder bldr, StructuralClass.ClassName type, ArrayList<String> adduct_types, boolean noExtraAdduct) throws Exception{
		ArrayList<String> adduct_types_valid = new ArrayList<String>();
		ArrayList<String> adduct_types_invalid = new ArrayList<String>();
		for(String adduct : adduct_types){
			if(FPLists.classSpecificFragmentationPatterns.get(type).keySet().contains(adduct)){
				adduct_types_valid.add(adduct);
			}else{
				adduct_types_invalid.add(adduct);
			}
		}
//		System.out.println(adduct_types_valid.size() < adduct_types.size());
		if(adduct_types_valid.size() < adduct_types.size()){
			if(adduct_types.size() == 1){	
				if(noExtraAdduct){
					System.out.println(
							"STATUS REPORT = 2\nThe following adducts are not covered for the class of " + type + ": " +
									Arrays.toString(adduct_types_invalid.toArray()) + "\n"
											+ "program terminates as the user selected No Extra Adduct Type option");
					return;
				}
				System.out.println(
					"STATUS REPORT = 3\nThe following adducts are not covered for the class of " + type + ": " +
							Arrays.toString(adduct_types_invalid.toArray()) + "\n"
									+ "Next Step: Predicting MS spectra for covered adduct types: "
									+ Arrays.toString(FPLists.classSpecificFragmentationPatterns.get(type).keySet().toArray())
						);
				saveSingleCfmidLikeMSPeakList(molecule, bldr, type, false);
			}
			else{
				System.out.println(
				"STATUS REPORT = 2\nThe following adducts are not covered for the class of " + type + ": " +
						Arrays.toString(adduct_types_invalid.toArray()) + "\n"
								+ "Next Step: Predicting MS spectra for remaining adduct types: "
								+ Arrays.toString(adduct_types_valid.toArray()));
				for(String adt : adduct_types_valid){
					//System.out.println("Generating peak list for the adduct type: " + String.valueOf(adt));
					LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<String>>> annotatedPeaks = new LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<String>>>();
					LinkedHashMap<String, IAtomContainer> fragments = this.fr.fragmentMolecule(molecule, type, adt);
					FragmentationCondition fragCondition_10 =  new FragmentationCondition(adt, 10);
					FragmentationCondition fragCondition_20 =  new FragmentationCondition(adt, 20);
					FragmentationCondition fragCondition_40 =  new FragmentationCondition(adt, 40);
					
					annotatedPeaks.put(10, this.fr.annotatePeakList(fragments ,type, fragCondition_10));
					annotatedPeaks.put(20,	this.fr.annotatePeakList(fragments ,type, fragCondition_20));
					annotatedPeaks.put(40, this.fr.annotatePeakList(fragments ,type, fragCondition_40));
					
					
					this.ffif.printSingleCfmidLikeMSAnnotatedPeakList(annotatedPeaks, adt);

				}				
			}
			
		} 
		else{
//				System.out.println("STATUS REPORT = 1\nEach specified adduct is covered for the class of " + type + ". "
//						+ "Next Step: Predicting MS spectra for the following adduct types: "
//						+ Arrays.toString(adduct_types_valid.toArray())
//				);
				for(String adduct : adduct_types){
					//System.out.println("Generating peak list for the adduct type: " + String.valueOf(adduct));
					LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<String>>> annotatedPeaks = new LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<String>>>();
					LinkedHashMap<String, IAtomContainer> fragments = this.fr.fragmentMolecule(molecule, type, adduct);
//					System.out.println(fragments.keySet().size());
//					for(String x : fragments.keySet()){
//						System.out.println(x);
//						System.out.println(fragments.get(x)==null);
//					}
					FragmentationCondition fragCondition_10 =  new FragmentationCondition(adduct, 10);
					FragmentationCondition fragCondition_20 =  new FragmentationCondition(adduct, 20);
					FragmentationCondition fragCondition_40 =  new FragmentationCondition(adduct, 40);
					
					annotatedPeaks.put(10, this.fr.annotatePeakList(fragments, type, fragCondition_10));
					annotatedPeaks.put(20,	this.fr.annotatePeakList(fragments, type, fragCondition_20));
					annotatedPeaks.put(40, this.fr.annotatePeakList(fragments, type, fragCondition_40));

					this.ffif.printSingleCfmidLikeMSAnnotatedPeakList(annotatedPeaks, adduct);

				}				
		}
	}

	public void saveSingleCfmidLikeMSPeakList(IAtomContainer molecule, IChemObjectBuilder bldr, StructuralClass.ClassName type, boolean standardize) throws Exception{
		
		IAtomContainer standardized_mol = molecule;
		if(standardize){
			standardized_mol = this.sExplorer.standardizeMolecule(molecule);
		}

		if(FPLists.classSpecificFragmentationPatterns.containsKey(type)){
			//System.out.println("\nSTATUS REPORT = 0\nPredicting spectra for all covered adduct types.");
			for(String adduct : FPLists.classSpecificFragmentationPatterns.get(type).keySet()){
				LinkedHashMap<String, FragmentationPattern> check = FPLists.classSpecificFragmentationPatterns.get(type);
				//System.out.println("Generating peak list for the adduct type: " + String.valueOf(adduct));
				LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<String>>> annotatedPeaks = new LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<String>>>();
				LinkedHashMap<String, IAtomContainer> fragments = this.fr.fragmentMolecule(standardized_mol, type, adduct);
				FragmentationCondition fragCondition_10 =  new FragmentationCondition(adduct, 10);
				FragmentationCondition fragCondition_20 =  new FragmentationCondition(adduct, 20);
				FragmentationCondition fragCondition_40 =  new FragmentationCondition(adduct, 40);
				
				annotatedPeaks.put(10, this.fr.annotatePeakList(fragments ,type, fragCondition_10));
				annotatedPeaks.put(20,	this.fr.annotatePeakList(fragments ,type, fragCondition_20));
				annotatedPeaks.put(40, this.fr.annotatePeakList(fragments ,type, fragCondition_40));
				
				this.ffif.printSingleCfmidLikeMSAnnotatedPeakList(annotatedPeaks, adduct);
				
			}			
		}
		else{
        	if(type == ClassName.GLYCEROLIPIDS || type == ClassName.GLYCEROPHOSPHOLIPIDS || type == ClassName.SPHINGOLIPIDS 
       			|| type == ClassName.CERAMIDE_1_PHOSPHATES || type == ClassName.DIPHOSPHORYLATED_HEXAACYL_LIPID_A ||
        			type == ClassName.SULFATIDES || type == ClassName.FATTY_ACID_ESTERS_OF_HYDROXYL_FATTY_ACIDS ||
        			type == ClassName.ETHER_LIPIDS
        			){            	
    			System.out.println("STATUS REPORT = 4\nThe compound belongs to a lipid class of " + type + ", which is not covered "
    					+ "in the current version of the fragmenter.");       		
        	}
        	else{
    			System.out.println("STATUS REPORT = 5\nInvalid chemical class. The query compound does not belong to any of the classes covered "
    					+ "in the current version of the fragmenter.");         		
       	}
        }
		
		

	}
	
	
}

