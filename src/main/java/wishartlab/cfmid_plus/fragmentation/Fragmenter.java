/**
 * 
 */
package wishartlab.cfmid_plus.fragmentation;

import java.io.BufferedWriter;
import java.io.File;
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
//import java.nio.file.Path;
//import java.nio.file.Paths;
//import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
//import java.util.List;
import java.util.Map;
//import java.util.StringJoiner;
import java.util.regex.Pattern;
//import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.openscience.cdk.aromaticity.Aromaticity;
import org.openscience.cdk.aromaticity.ElectronDonation;
//import org.openscience.cdk.AtomContainer;
//import org.openscience.cdk.AtomContainerSet;
//import org.openscience.cdk.CDKConstants;
//import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.graph.Cycles;
import org.openscience.cdk.interfaces.IAtom;
//import org.openscience.cdk.exception.InvalidSmilesException;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IAtomContainerSet;
import org.openscience.cdk.interfaces.IChemObjectBuilder;
//import org.openscience.cdk.interfaces.IMolecularFormula;
//import org.openscience.cdk.io.iterator.IteratingSDFReader;
import org.openscience.cdk.silent.SilentChemObjectBuilder;
import org.openscience.cdk.smiles.SmiFlavor;
import org.openscience.cdk.smiles.SmilesGenerator;
import org.openscience.cdk.smiles.SmilesParser;
import org.openscience.cdk.tools.CDKHydrogenAdder;
//import org.openscience.cdk.smiles.smarts.SmartsPattern;
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator;
//import org.openscience.cdk.tools.manipulator.MolecularFormulaManipulator;

import ambit2.smarts.SMIRKSManager;
import ambit2.smarts.SMIRKSReaction;
import main.RuleBasedFrag;
import utils.CorrectRAtoms;
//import ambit2.smarts.query.SMARTSException;
import wishartlab.cfmid_plus.molecules.StructuralClass;
import wishartlab.cfmid_plus.molecules.StructuralClass.ClassName;
//import wishartlab.cfmid_plus.fragmentation.FragmentationPattern;

/**
 * @author Yannick Djoumbou Feunang
 *
 */

import wishartlab.cfmid_plus.molecules.StructureExplorer;






/**
 * This class describes the Fragmenter, which takes a molecule and returns the possible fragments according
 * to more elaborate fragmentation patterns
 * 
 * References:
 * R1: Murphy RC et al (2001) Chem. Rev. 2001, 101, 479−526
 * 
 * 
 * @author yandj
 *
 */

/**
 */



public class Fragmenter {

	
	IChemObjectBuilder bldr = SilentChemObjectBuilder.getInstance();
	private SMIRKSManager smrkMan = new SMIRKSManager(bldr);
	public String SMILES = "";
	public String InChiKey  = "";


//	private AtomContainerManipulator acm = new AtomContainerManipulator();
	private SmilesParser sParser = new SmilesParser(SilentChemObjectBuilder.getInstance());
	private SmilesGenerator sGen = SmilesGenerator.unique();//SmilesGenerator.isomeric();
//	private String host;
//	private int port;
	StructureExplorer sExplorer = new StructureExplorer();

	public Fragmenter(){
//		this.host = "127.0.0.1";
//		this.port = 6311;
		this.smrkMan.setFlagFilterEquivalentMappings(true);
		this.smrkMan.setFlagApplyStereoTransformation(true);
		//this.smrkMan.setFlagAromaticityTransformation(true);
		
		//this.smrkMan.set
	}
	
	
	public LinkedHashMap<String, IAtomContainer>  fragmentMolecule(IAtomContainer molecule, StructuralClass.ClassName type) throws Exception{
		return fragmentMolecule(molecule, type, "[M+H]+");
	}
	
	public boolean isConditionValidForChemClass(StructuralClass.ClassName type,
			String adductType){
		boolean valid = false;
		
		if(FPLists.classSpecificFragmentationPatterns.get(type).containsKey(adductType)){
			valid = true;
		}
		
		return valid;
	}
	
	public LinkedHashMap<String, IAtomContainer> fragmentMolecule(IAtomContainer molecule, StructuralClass.ClassName type, String adductType) throws Exception{	
		//System.out.println("Chemical Class = " + StructureExplorer.findClassName(molecule));
		LinkedHashMap<String, IAtomContainer> fragments = new LinkedHashMap<String, IAtomContainer>();	
		SmilesParser sp = new SmilesParser(SilentChemObjectBuilder.getInstance());
		if(type != StructuralClass.ClassName.NIL && FPLists.classSpecificFragmentationPatterns.get(type).containsKey(adductType)){
			//new SmilesGenerator();
			//			IAtomContainer stMol = this.sExplorer.standardizeMolecule(molecule);
			//SmilesGenerator sg = SmilesGenerator.isomeric();
			SmilesGenerator sg = new SmilesGenerator(SmiFlavor.Isomeric);
			for(Map.Entry<String, String[]> pattern : FPLists.classSpecificFragmentationPatterns.get(type).get(adductType).patterns.entrySet()){
				SMIRKSReaction sReaction = this.smrkMan.parse(pattern.getValue()[1]);
				if(pattern.getKey().equals("[M+H]+Y0")){
//					System.out.println("Check Point");
				}
				CorrectRAtoms.assignAtomIdx(molecule);
				IAtomContainerSet current_set = this.smrkMan.applyTransformationWithSingleCopyForEachPos(molecule, null, sReaction);	

				if(current_set != null){
					for(int t = 0; t < current_set.getAtomContainerCount(); t++) {
						CorrectRAtoms.reMapRAtoms(current_set.getAtomContainer(t), molecule);
					}
					IAtomContainerSet current_set_unique = StructureExplorer.uniquefy(current_set);
					
					for(IAtomContainer atc : current_set_unique.atomContainers()){
						 IAtomContainerSet partitions = StructureExplorer.partition(atc);
						 	for(IAtomContainer partition : partitions.atomContainers()){
						 		//System.out.println(pattern.getValue()[0]);
						 		//IAtomContainer temp  = sp.parseSmiles(sg.create(partition));
						 		//AtomContainerManipulator.suppressHydrogens(temp);
						 		//System.out.println(sg.create(partition));
						 		if(StructureExplorer.containsSmartsPattern(partition, pattern.getValue()[0])){ //partition
//								if(this.sExplorer.containsSmartsPattern(this.sExplorer.standardizeMolecule(partition), pattern.getValue()[0])){
									if(pattern.getValue().length==2){
										fragments.put(pattern.getKey(), AtomContainerManipulator.removeHydrogens(partition));
//										System.out.println(sg.create(partition) + " was added.");
										
									}else if (pattern.getValue().length==3){
										// take the SMILES of the fragment an add the adduct, whiich would be the 
										// third element in the array.
										IAtomContainer  adjustedPartition = this.sParser.parseSmiles(sg.create(AtomContainerManipulator.removeHydrogens(partition))
												+ "." + pattern.getValue()[2]);	
										fragments.put(pattern.getKey(), adjustedPartition);
//										System.out.println(sg.create(adjustedPartition) + " was added.");		
									}

									break;	
								} else{
//									System.out.println(sg.create(partition) + " was NOT added.");
								}			
						 	}
						}
					} else{
						// TO DO
					}				
				}

				return fragments;
			} else
				System.err.println("WHAT IS GOING ON?");
				return null;
		
		}
	
	public LinkedHashMap<String, Double> computeFragmentMasses(LinkedHashMap<String, IAtomContainer> fragments){
		LinkedHashMap<String, Double> fmasses = new LinkedHashMap<String, Double>();
		
		if(fragments != null){
			for(Map.Entry<String, IAtomContainer> fragment :  fragments.entrySet()){
				//System.err.println(fragment.getKey());
				fmasses.put(fragment.getKey(), 
						Math.floor(StructureExplorer.getMajorIsotopeMass(fragment.getValue()) * 100000)/100000);		
			}
		}else{
			fmasses = null;
		}
		
		return fmasses;		
	}
	public LinkedHashMap<String, Double> computeFragmentMassChargeRatios(LinkedHashMap<String, IAtomContainer> fragments) throws CDKException{
		LinkedHashMap<String, Double> fmassToChargeRatios = new LinkedHashMap<String, Double>();
		
		if(fragments != null){
			for(Map.Entry<String, IAtomContainer> fragment :  fragments.entrySet()){
//				System.err.println(fragment.getKey());
//				System.out.println(this.sGen.create(fragment.getValue()));
				int z = (int) Math.abs(AtomContainerManipulator.getTotalFormalCharge(fragment.getValue()));
//				System.err.println("Z = " + z);
				if(z==0){
					fmassToChargeRatios.put(fragment.getKey(), 
							Math.floor(StructureExplorer.getMajorIsotopeMass(fragment.getValue()) * 100000)/100000);						
				} else{
					fmassToChargeRatios.put(fragment.getKey(), 
							Math.floor(StructureExplorer.getMajorIsotopeMass(fragment.getValue()) * 100000)/ (z * 100000));	
				}
	
			}
		}else{
			fmassToChargeRatios = null;
		}
		
		return fmassToChargeRatios;		
	}	
	
	public LinkedHashMap<String, ArrayList<String>>  annotatePeakList(LinkedHashMap<String, IAtomContainer> fragments ,ClassName type, FragmentationCondition fragCondition) throws Exception{
		
		if(fragments == null || type == null || fragCondition == null){
			return null;
		} else{
			LinkedHashMap<String, ArrayList<String>> results = new LinkedHashMap<String, ArrayList<String>>();
			ArrayList<String> frag_smiles_mass = new ArrayList<String>();
			ArrayList<String> peaks = new ArrayList<String>();								
			LinkedHashMap<String, Double> fragmentMassChargeRatios = computeFragmentMassChargeRatios(fragments);
			LinkedHashMap<Double, ArrayList<String>> massesToLabel= new LinkedHashMap<Double, ArrayList<String>>();
			ArrayList<String> labels = new ArrayList<String>(fragmentMassChargeRatios.keySet());
			LinkedHashMap<String, MSPeakRelativeAbundance> check =  MSPRelativeAbundanceList.classSpecificRelativeAbundances.get(type);
			MSPeakRelativeAbundance mra = MSPRelativeAbundanceList.classSpecificRelativeAbundances.get(type).get(fragCondition.adductName + "_" + fragCondition.collisionEnergy);		
			/**
			 * For the polyphenol classes I added, I use the code below to adjust the mass of having/losing the hydrogen.
			 * Note that I used neutral forms in the fragmentation rules for those classes. Thus we have to adjust the mass with this extra code.
			 * 
			 */
			if(type == ClassName.FLAVONOL || type == ClassName.FLAVONE || type == ClassName.FLAVANONE || 
			   type == ClassName.FLAVONOID_3_O_GLYCOSIDES_AGLYCONE_FLAVONOL_ONESUGAR || type == ClassName.FLAVONOID_3_O_GLYCOSIDES_AGLYCONE_FLAVONOL_MULTISUGAR ||
			   type == ClassName.FLAVONOID_7_O_GLYCOSIDES_AGLYCONE_FLAVONE_ONESUGAR ||  type == ClassName.FLAVONOID_7_O_GLYCOSIDES_AGLYCONE_FLAVONE_MULTISUGAR ||
			   type == ClassName.FLAVONOID_7_O_GLYCOSIDES_AGLYCONE_FLAVANONE_ONESUGAR || type == ClassName.FLAVONOID_7_O_GLYCOSIDES_AGLYCONE_FLAVANONE_MULTISUGAR || 
			   type == ClassName.FLAVONOID_7_O_GLUCURONIDES_AGLYCONE_FLAVONE ||
			   type == ClassName._4_O_METHYLATED_FLAVONOIDS_AGLYCONE_FLAVANONE || type == ClassName._7_O_METHYLATED_FLAVONOIDS_AGLYCONE_FLAVANONE ||
			   type == ClassName._3_O_METHYLATED_FLAVONOIDS_AGLYCONE_FLAVANONE || type == ClassName._4_O_METHYLATED_FLAVONOIDS_AGLYCONE_FLAVONE ||
			   type == ClassName._7_O_METHYLATED_FLAVONOIDS_AGLYCONE_FLAVONE){
			
				for(String key : fragmentMassChargeRatios.keySet()){
					Double mass = fragmentMassChargeRatios.get(key);
					if(key.contains("[M-H]-")){
						fragmentMassChargeRatios.put(key, mass - 1.00727647); 
					}		
				}
				
			}
				
			for(Map.Entry<String, Double> fm : fragmentMassChargeRatios.entrySet()){
				if(mra.getRelativeAbundances().containsKey(fm.getKey())){
					if(massesToLabel.containsKey(fm.getValue())){
						massesToLabel.get(fm.getValue()).add(fm.getKey());
					}else{
						massesToLabel.put(fm.getValue(), new ArrayList<String>());
						massesToLabel.get(fm.getValue()).add(fm.getKey());
					}
				}
			}			
			for(Double mass : massesToLabel.keySet()){
					if( massesToLabel.get(mass).size() == 1 && mra.getRelativeAbundances().containsKey(massesToLabel.get(mass).get(0))){
						peaks.add(String.format("%.5f %.1f %d (1)",mass, mra.getRelativeAbundances().get(massesToLabel.get(mass).get(0)), labels.indexOf(massesToLabel.get(mass).get(0))));
					} else{
	
						ArrayList<String> indexes = new ArrayList<String>();
						ArrayList<String> scores = new ArrayList<String>();
						
						for(String x : massesToLabel.get(mass)){
							if(mra.getRelativeAbundances().containsKey(x)){
								indexes.add(String.valueOf(labels.indexOf(x)));
								scores.add("1");
							}
						}
						peaks.add(String.format("%.5f %.1f %8s",mass, mra.getRelativeAbundances().get(massesToLabel.get(mass).get(0)), 
						StringUtils.join(indexes," ")) + " (" + StringUtils.join(" ", scores) + ")");				
					}
			}
				/*
				 * Add Fragment masses and structures
				 */					
				for(Map.Entry<String, IAtomContainer> frag : fragments.entrySet()){
					//System.out.println(this.sGen.create(fragments.get(frag.getKey())));
					frag_smiles_mass.add(String.format("%.5f", fragmentMassChargeRatios.get(frag.getKey())) + " " + this.sGen.create(fragments.get(frag.getKey())));
				}									
			results.put("peaks_list", peaks);
			results.put("fragments", frag_smiles_mass);
			return results;
		}
	}
	
	public LinkedHashMap<String, ArrayList<String>> generateCfmidLikeMSPeakList(IAtomContainer molecule, FragmentationCondition fragCondition) throws Exception{
//		LinkedHashMap<String, ArrayList<String>> annotatedPeaks = new LinkedHashMap<String, ArrayList<String>>();
		
		if(fragCondition.getCollisionEnergy() == 10 || fragCondition.getCollisionEnergy() == 20 || fragCondition.getCollisionEnergy() == 40) {
			IAtomContainer standardized_mol = this.sExplorer.standardizeMolecule(molecule);
//			System.out.println("The standardized molecule is " + this.sGen.create(standardized_mol));
			StructuralClass.ClassName type = StructureExplorer.findClassName(standardized_mol);
//			System.out.println("The type of this molecule is " + String.valueOf(type));
			LinkedHashMap<String, IAtomContainer> fragments = fragmentMolecule(standardized_mol, type, fragCondition.getAdductName());
			
			return  annotatePeakList(fragments ,type, new FragmentationCondition(fragCondition.getAdductName(), 10));
		} else {
			
			throw new IllegalArgumentException("The collision energy must be either 10 eV, 20 eV, or 40 eV.\nPlease enter a valid collision energy");
		}
	}
	

	
	public LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<String>>> generateCfmidLikeMSPeakList(IAtomContainer molecule, String adductType) throws Exception{
		LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<String>>> annotatedPeaks = new LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<String>>>();
		
		IAtomContainer standardized_mol = this.sExplorer.standardizeMolecule(molecule);
		StructuralClass.ClassName type = StructureExplorer.findClassName(standardized_mol);
//		System.out.println("The type of this molecule is " + String.valueOf(type));
		LinkedHashMap<String, IAtomContainer> fragments = fragmentMolecule(standardized_mol, type, adductType);
		FragmentationCondition fragCondition_10 =  new FragmentationCondition(adductType, 10);
		FragmentationCondition fragCondition_20 =  new FragmentationCondition(adductType, 20);
		FragmentationCondition fragCondition_40 =  new FragmentationCondition(adductType, 40);
		
		annotatedPeaks.put(10, annotatePeakList(fragments ,type, fragCondition_10));
		annotatedPeaks.put(20,	annotatePeakList(fragments ,type, fragCondition_20));
		annotatedPeaks.put(40, annotatePeakList(fragments ,type, fragCondition_40));
		
		return annotatedPeaks;
	}

	public void saveSingleCfmidLikeMSPeakList(IAtomContainer molecule, IChemObjectBuilder bldr, String outputname, ArrayList<String> adduct_types, boolean noExtraAdduct) throws Exception{	
		IAtomContainer standardized_mol = this.sExplorer.standardizeMolecule(molecule);
		StructuralClass.ClassName type = StructureExplorer.findClassName(standardized_mol);
//		System.out.println("The type of this molecule is " + String.valueOf(type));

        if(FPLists.classSpecificFragmentationPatterns.containsKey(type)){
            saveSingleCfmidLikeMSPeakList(standardized_mol, bldr, outputname, type, adduct_types, noExtraAdduct);  	
        } 
        else{
        	
        	if(type == ClassName.GLYCEROLIPIDS || type == ClassName.GLYCEROPHOSPHOLIPIDS || type == ClassName.SPHINGOLIPIDS 
        			|| type == ClassName.CERAMIDE_1_PHOSPHATES || type == ClassName.DIPHOSPHORYLATED_HEXAACYL_LIPID_A ||
        			type == ClassName.SULFATIDES || type == ClassName.FATTY_ACID_ESTERS_OF_HYDROXYL_FATTY_ACIDS ||
        			type == ClassName.ETHER_LIPIDS
        			){            	
    			System.out.println("STATUS REPORT = 4\nThe compound belongs to the lipid class of " + type + ", which is not covered "
    					+ "in the current version of the fragmenter.");       		
        	}
        	else{
    			System.out.println("STATUS REPORT = 5\nInvalid chemical class. The query compound does not belong to any of the classes covered "
    					+ "in the current version of the fragmenter.");         		
        	}

        }
	}
	
	public void saveSingleCfmidLikeMSPeakList(IAtomContainer molecule, IChemObjectBuilder bldr, String outputname, StructuralClass.ClassName type, ArrayList<String> adduct_types, boolean noExtraAdduct) throws Exception{
		ArrayList<String> adduct_types_valid = new ArrayList<String>();
		ArrayList<String> adduct_types_invalid = new ArrayList<String>();
		for(String adduct : adduct_types){
			if(FPLists.classSpecificFragmentationPatterns.get(type).keySet().contains(adduct)){
				adduct_types_valid.add(adduct);
			}else{
				adduct_types_invalid.add(adduct);
			}
		}
//		System.out.println(adduct_types_valid.size() < adduct_types.size());
		if(adduct_types_valid.size() < adduct_types.size()){
			if(adduct_types.size() == 1){	
				if(noExtraAdduct){
					System.out.println(
							"STATUS REPORT = 2\nThe following adducts are not covered for the class of " + type + ": " +
									Arrays.toString(adduct_types_invalid.toArray()) + "\n"
											+ "program terminates as the user selected No Extra Adduct Type option");
					return;
				}
				System.out.println(
					"STATUS REPORT = 3\nThe following adducts are not covered for the class of " + type + ": " +
							Arrays.toString(adduct_types_invalid.toArray()) + "\n"
									+ "Next Step: Predicting MS spectra for covered adduct types: "
									+ Arrays.toString(FPLists.classSpecificFragmentationPatterns.get(type).keySet().toArray())
						);
				saveSingleCfmidLikeMSPeakList(molecule, bldr, type, outputname, false);
			}
			else{
				System.out.println(
				"STATUS REPORT = 2\nThe following adducts are not covered for the class of " + type + ": " +
						Arrays.toString(adduct_types_invalid.toArray()) + "\n"
								+ "Next Step: Predicting MS spectra for remaining adduct types: "
								+ Arrays.toString(adduct_types_valid.toArray()));
				for(String adt : adduct_types_valid){
					//System.out.println("Generating peak list for the adduct type: " + String.valueOf(adt));
					LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<String>>> annotatedPeaks = new LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<String>>>();
					LinkedHashMap<String, IAtomContainer> fragments = fragmentMolecule(molecule, type, adt);
					FragmentationCondition fragCondition_10 =  new FragmentationCondition(adt, 10);
					FragmentationCondition fragCondition_20 =  new FragmentationCondition(adt, 20);
					FragmentationCondition fragCondition_40 =  new FragmentationCondition(adt, 40);
					
					annotatedPeaks.put(10, annotatePeakList(fragments ,type, fragCondition_10));
					annotatedPeaks.put(20,	annotatePeakList(fragments ,type, fragCondition_20));
					annotatedPeaks.put(40, annotatePeakList(fragments ,type, fragCondition_40));
					
					File outfile = new File(outputname);
					String name = outfile.getName();
					String parentDir = outfile.getParent();
					
					
					String[] nameSplit = name.split(Pattern.quote("."));
					String fullname = null;
					if(nameSplit.length > 1){
						if (parentDir != null){
							//fullname = parentDir + "/" + nameSplit[0] + "_" + adt + "." + nameSplit[1];
							fullname = parentDir + "/" + name + "_" + adt + ".log";
						}
						else {
							//fullname = nameSplit[0] + "_" + adt + "." + nameSplit[1];
							fullname = name + "_" + adt + ".log";
						}
						
					}
					else if(nameSplit.length == 1){
						if (parentDir != null){
							//fullname = parentDir + "/" + nameSplit[0] + "_" + adt;
							fullname = parentDir + "/" + name + "_" + adt +".log";
						}
						else {
							//fullname = nameSplit[0] + "_" + adt;
							fullname = name + "_" + adt +".log";
						}
						
					}
					
//					System.err.println("fullname: " + fullname);
					saveSingleCfmidLikeMSAnnotatedPeakList(annotatedPeaks, adt, fullname);
					System.out.println("Saving spectrum to " + fullname);
				}				
			}
			
		} else{
				System.out.println("STATUS REPORT = 1\nEach specified adduct is covered for the class of " + type + ". "
						+ "Next Step: Predicting MS spectra for the following adduct types: "
						+ Arrays.toString(adduct_types_valid.toArray())
				);
				for(String adduct : adduct_types){
					//System.out.println("Generating peak list for the adduct type: " + String.valueOf(adduct));
					LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<String>>> annotatedPeaks = new LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<String>>>();
					LinkedHashMap<String, IAtomContainer> fragments = fragmentMolecule(molecule, type, adduct);
//					System.out.println(fragments.keySet().size());
//					for(String x : fragments.keySet()){
//						System.out.println(x);
//						System.out.println(fragments.get(x)==null);
//					}
					FragmentationCondition fragCondition_10 =  new FragmentationCondition(adduct, 10);
					FragmentationCondition fragCondition_20 =  new FragmentationCondition(adduct, 20);
					FragmentationCondition fragCondition_40 =  new FragmentationCondition(adduct, 40);
					
					annotatedPeaks.put(10, annotatePeakList(fragments, type, fragCondition_10));
					annotatedPeaks.put(20,	annotatePeakList(fragments, type, fragCondition_20));
					annotatedPeaks.put(40, annotatePeakList(fragments, type, fragCondition_40));
					
					File outfile = new File(outputname);
					String name = outfile.getName();
					String parentDir = outfile.getParent();
					
					
					String[] nameSplit = name.split(Pattern.quote("."));
					//System.err.println("c: " + parentDir);
					String fullname = null;
					if(nameSplit.length > 1){
						if (parentDir != null){
							//fullname = parentDir + "/" + nameSplit[0] + "_" + adduct + "." + nameSplit[1];
							fullname = parentDir + "/" + name + "_" + adduct + ".log";
						}
						else {
							fullname = name + "_" + adduct + ".log";
						}
						
					}
					else if(nameSplit.length == 1){
						if (parentDir != null){
							fullname = parentDir + "/" + name + "_" + adduct;
						}
						else {
							fullname = nameSplit[0] + "_" + adduct;
						}
						
					}
					
//					System.err.println("fullname: " + fullname);
					saveSingleCfmidLikeMSAnnotatedPeakList(annotatedPeaks, adduct, fullname);
					System.out.println("Saving spectrum to " + fullname);
				}				
		}
	}

	

	public void saveSingleCfmidLikeMSPeakList(IAtomContainer molecule, IChemObjectBuilder bldr, String outputname) throws Exception{
		IAtomContainer standardized_mol = this.sExplorer.standardizeMolecule(molecule);
		StructuralClass.ClassName type = StructureExplorer.findClassName(standardized_mol);
		saveSingleCfmidLikeMSPeakList(standardized_mol, bldr, type, outputname, false);
	
	}

	
	public void saveSingleCfmidLikeMSPeakList(IAtomContainer molecule, IChemObjectBuilder bldr, StructuralClass.ClassName type, String outputname, boolean standardize) throws Exception{
		
		IAtomContainer standardized_mol = molecule;
		if(standardize){
			standardized_mol = this.sExplorer.standardizeMolecule(molecule);
		}

		if(FPLists.classSpecificFragmentationPatterns.containsKey(type)){		
			boolean printMSG = true;
			for(String adduct : FPLists.classSpecificFragmentationPatterns.get(type).keySet()){
				LinkedHashMap<String, FragmentationPattern> check = FPLists.classSpecificFragmentationPatterns.get(type);
				//System.out.println("Generating peak list for the adduct type: " + String.valueOf(adduct));
				LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<String>>> annotatedPeaks = new LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<String>>>();
				LinkedHashMap<String, IAtomContainer> fragments = fragmentMolecule(standardized_mol, type, adduct);
				FragmentationCondition fragCondition_10 =  new FragmentationCondition(adduct, 10);
				FragmentationCondition fragCondition_20 =  new FragmentationCondition(adduct, 20);
				FragmentationCondition fragCondition_40 =  new FragmentationCondition(adduct, 40);
				
				annotatedPeaks.put(10, annotatePeakList(fragments ,type, fragCondition_10));
				annotatedPeaks.put(20,	annotatePeakList(fragments ,type, fragCondition_20));
				annotatedPeaks.put(40, annotatePeakList(fragments ,type, fragCondition_40));
				if(annotatedPeaks==null || annotatedPeaks.isEmpty() || !isValidAnnotatedPeaksList(annotatedPeaks)) {
					System.out.println("STATUS REPORT = 7\\nNo results produced by msrb.");
					return;
				}
				if(printMSG) {
					System.out.println("\nSTATUS REPORT = 0\nPredicting spectra for all covered adduct types.");
					printMSG = false;
				}
				File outfile = new File(outputname);
				String name = outfile.getName();
				String parentDir = outfile.getParent();
				
				
				String[] nameSplit = name.split(Pattern.quote("."));
				String fullname = null;
				if(nameSplit.length > 1){
					if (parentDir != null){
						//fullname = parentDir + "/" + nameSplit[0] + "_" + adduct + "." + nameSplit[1];
						fullname = parentDir + "/" + name + "_" + adduct + ".log";
					}
					else {
						//fullname = nameSplit[0] + "_" + adduct + "." + nameSplit[1];
						fullname = name + "_" + adduct + ".log";
					}
					
				}
				else if(nameSplit.length == 1){
					if (parentDir != null){
						//fullname = parentDir + "/" + nameSplit[0] + "_" + adduct;
						fullname = parentDir + "/" + name + "_" + adduct + ".log";
					}
					else {
						//fullname = nameSplit[0] + "_" + adduct;
						fullname = nameSplit[0] + "_" + adduct + ".log";
					}
					
				}
				
//				System.err.println("fullname: " + fullname);
				saveSingleCfmidLikeMSAnnotatedPeakList(annotatedPeaks, adduct, fullname);
				System.out.println("Saving spectrum to " + fullname);				
			}			
		}
		else{
        	if(type == ClassName.GLYCEROLIPIDS || type == ClassName.GLYCEROPHOSPHOLIPIDS || type == ClassName.SPHINGOLIPIDS 
       			|| type == ClassName.CERAMIDE_1_PHOSPHATES || type == ClassName.DIPHOSPHORYLATED_HEXAACYL_LIPID_A ||
        			type == ClassName.SULFATIDES || type == ClassName.FATTY_ACID_ESTERS_OF_HYDROXYL_FATTY_ACIDS ||
        			type == ClassName.ETHER_LIPIDS
        			){            	
    			System.out.println("STATUS REPORT = 4\nThe compound belongs to a lipid class of " + type + ", which is not covered "
    					+ "in the current version of the fragmenter.");       		
        	}
        	else{
    			System.out.println("STATUS REPORT = 5\nInvalid chemical class. The query compound does not belong to any of the classes covered "
    					+ "in the current version of the fragmenter.");         		
       	}
        }
		
		

	}
	
	
	
	public void saveSingleCfmidLikeMSAnnotatedPeakList(LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<String>>>annotatedPeaks, String adductType, String outputname) throws IOException{
		
		if(annotatedPeaks != null) {			
			FileWriter fw = new FileWriter(outputname);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write("#In-silico ESI-MS/MS" + " " + adductType + "  " + "Spectra");	
			bw.newLine();
			bw.write("#PREDICTED BY MSRB " + RuleBasedFrag.VERSION + "\n");
			bw.write("#SMILES=" + this.SMILES + "\n");
			bw.write("#InChiKey="  + this.InChiKey + "\n");
			int level = 0;
			int fragIndex = 0;
			
			for(Map.Entry<Integer, LinkedHashMap<String, ArrayList<String>>> peaks : annotatedPeaks.entrySet()){
				bw.write("energy" + level);
				bw.newLine();
				level++;
				for(String s : peaks.getValue().get("peaks_list")){
					bw.write(s);
					bw.newLine();
				}
			}
			
			bw.newLine();
			for( String fg : annotatedPeaks.get(10).get("fragments")){
				bw.write( fragIndex + " " + fg);
				bw.newLine();
				fragIndex++;
			}

			bw.newLine();
			bw.newLine();
			
			bw.close();	
			fw.close();
		}		
	}
	
	
	public boolean isValidAnnotatedPeaksList(LinkedHashMap<Integer, LinkedHashMap<String, ArrayList<String>>> annotatedPeaks) {
		for(Integer energy : annotatedPeaks.keySet()) {
			LinkedHashMap<String, ArrayList<String>> peaksMap = annotatedPeaks.get(energy);
			if(peaksMap == null || peaksMap.isEmpty()) continue;
			for(String peak : peaksMap.keySet()) {
				ArrayList<String> fragmentList = peaksMap.get(peak);
				if(fragmentList != null && !fragmentList.isEmpty()) {
					return true;
				}
			}
		}
		return false;
	}
	

}

