package wishartlab.cfmid_plus.molecules;

public class ReactionObject {
	private String name;
	private String reactantSMIRKS;
	private String reactionSMIRKS;
	
	public ReactionObject(String name, String reactantSMIRKS, String reactionSMIRKS) {
		super();
		this.name = name;
		this.reactantSMIRKS = reactantSMIRKS;
		this.reactionSMIRKS = reactionSMIRKS;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getReactantSMIRKS() {
		return reactantSMIRKS;
	}
	public void setReactantSMIRKS(String reactantSMIRKS) {
		this.reactantSMIRKS = reactantSMIRKS;
	}
	public String getReactionSMIRKS() {
		return reactionSMIRKS;
	}
	public void setReactionSMIRKS(String reactionSMIRKS) {
		this.reactionSMIRKS = reactionSMIRKS;
	}
	
}
