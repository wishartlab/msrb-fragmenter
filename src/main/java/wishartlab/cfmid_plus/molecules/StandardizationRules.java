/**
 * Theses rules are obtained from BioTransformer's standardization progress
 * @author of BioTransformer is Djoumbou Feunang, Yannick, PhD
 */
package wishartlab.cfmid_plus.molecules;

import java.util.ArrayList;

public class StandardizationRules {
	public static ArrayList<ReactionObject> standardizationReactions;
	static{
		standardizationReactions = new ArrayList<>();
		standardizationReactions.add(new ReactionObject("AZIDE_STANDARDIZATION", "[#7;A;X2-][N;X2+]#[N;X1]", "[#7;A;X2-:1][N;X2+:2]#[N;X1:3]>>[#7:1]=[N+:2]=[#7-:3]"));
		standardizationReactions.add(new ReactionObject("CARBOXAMIDE_STANDARDIZATION_PATTERN1", "[#6]-[#6;X3]([#8;A;X2H1])=[#7;$([#7;X2]-[#6]),$([#7;X2H1])]", "[H:5][#8;X2:3][#6;A;X3:1]([#6:2])=[#7;$([#7;X2]-[#6]),$([#7;X2H1]):4]>>[H:5][#7;A:4][#6;A;X3:1]([#6:2])=[O;X1:3]"));
		standardizationReactions.add(new ReactionObject("CARBOXAMIDE_STANDARDIZATION_PATTERN2", "[#6]-[#6;X3]([#8;A;X1-])=[#7;$([#7;X2]-[#6]),$([#7;X2H1])]", "[#6:1]-[#6;X3:2]([#8;A;X1-:3])=[#7;$([#7;X2]-[#6]),$([#7;X2H1]):4]>>[#6:1]-[#6;X3:2](-[#7;X3:4])=[#8;A;X1:3]"));
		standardizationReactions.add(new ReactionObject("DIAZO_STANDARDIZATION", "[#6;X3-][N;X2+]#[N;X1]", "[#6;A;X3-:1][N;X2+:2]#[N;X1:3]>>[#6;A:1]=[N+:2]=[#7-:3]"));
		standardizationReactions.add(new ReactionObject("DIAZONIUM_STANDARDIZATION", "[#6]-[#7]=[#7+]","[#6]-[#7:1]=[#7+:2]>>[#6][N+:1]#[N:2]"));
		standardizationReactions.add(new ReactionObject("IMINIUM_STANDARDIZATION", "[#6;A;X3+][#7;A;X3]", "[#6;A;X3+:1][#7;A;X3:2]>>[#6;A:1]=[#7+:2]"));
		standardizationReactions.add(new ReactionObject("ISOCYANATE_STANDARDIZATION", "[H][#7][#6;A;X2H0;v3-]=O", "[H][#7:1][#6;A;X2H0;v3-:2]=[O:3]>>[#7;v3:1]=[C;X2;v4:2]=[O:3]"));
		standardizationReactions.add(new ReactionObject("NITRILIUM_STANDARDIZATION", "[#6;A;X2+]=[#7;X2]","[#6;A;X2+:1]=[#7;X2:2]>>[C:1]#[N+:2]" ));
		standardizationReactions.add(new ReactionObject("NITRO_STANDARDIZATION", "O=[N;X3+0,X3-]=[OX1-,OX1+0]", "[O:3]=[N;X3+0,X3-:1]=[OX1-,OX1+0:2]>>[#8-:2]-[#7+:1]=[O:3]"));		
		standardizationReactions.add(new ReactionObject("NITRO_STANDARDIZATION_1", "[H][#8;A;X2][#7;A;X4H1,X3+,X3-]([#6])=[O;X1]","[H][#8;A;X2:2][#7;A;X4H1,X3+,X3-:1]([#6])=[O;X1:3]>>[#6][#7;A;X3+:1]([#8;A;X1-:3])=[O:2]"));
		standardizationReactions.add(new ReactionObject("NITRONATE_STANDARDIZATION", "[#8;X1-]-[#7;X3+](-[#8;X1-])=[#6;A]", "[#8;X1-:1]-[#7;X3+:2](-[#8;X1-:3])=[#6;A:4]>>[H][#6;A:4][#7;X3+:2](-[#8;X1-:3])=[O;X1:1]"));
		standardizationReactions.add(new ReactionObject("NITROSO_STANDARDIZATION", "[#6]-[#7H2+]-[#8;X1-]", "[#6]-[#7H2+:1]-[#8;X1-:2]>>[#6]-[#7:1]=[O:2]"));		
		standardizationReactions.add(new ReactionObject("PHOSPHONIC_STANDARDIZATION", "[#6][P+]([#8;X2])([#8;X2])[#8-]", "[#6][P+:1]([#8;X2])([#8;X2])[#8-:2]>>[#6][P:1]([#8])([#8])=[O:2]"));
		standardizationReactions.add(new ReactionObject("PHOSPHORIC_STANDARDIZATION", "[#8;X1-][P;X4](=[O;X1])([#8;X2]-[*,#1])[#8;X2]-[*,#1]", "[#8;X1-:1][P;X4:2](=[O;X1:3])([#8;X2:6]-[*,#1:7])[#8;X2:4]-[*,#1:5]>>[H][#8;X2:1][P;X4:2](=[O;X1:3])([#8;X2:6]-[*,#1:7])[#8;X2:4]-[*,#1:5]"));
		standardizationReactions.add(new ReactionObject("PHOSPHORIC_STANDARDIZATION_2", "[#8;X1-][P;X4]([#8;X1-])(=[O;X1])[#8;X2]-[*,#1]", "[#8;X1-:3][P;X4:2]([#8;X1-:4])(=[O;X1:5])[#8;X2:1]-[*,#1:6]>>[H][#8;X2:3][P;X4:2](=[O;X1:5])([#8;X2:4][H])[#8;X2:1]-[*,#1:6]"));
		standardizationReactions.add(new ReactionObject("PHOSPHONIUM_YLIDE_STANDARDIZATION","[#6][P-]([#6])([#6])[#6+]", "[#6][P-:1]([#6])([#6])[#6+:2]>>[#6][P:1]([#6])([#6])=[#6;A:2]" ));
		standardizationReactions.add(new ReactionObject("SELENITE_STANDARDIZATION", "[#8;X2][Se+]([#8;X2])[#8-]", "[#8;X2][Se+:1]([#8;X2])[#8-:2]>>[#8][Se:1]([#8])=[O:2]"));
		standardizationReactions.add(new ReactionObject("SILICATE_STANDARDIZATION", "[#8;X2]-[#14+](-[#8;X2])-[#8-]","[#8;X2]-[#14+:1](-[#8;X2])-[#8-:2]>>[#8]-[#14:1](-[#8])=[O:2]" ));
		standardizationReactions.add(new ReactionObject("SULFINE_STANDARDIZATION", "[#6]-[#6](-[#6])=[S+][#8-]", "[#6]-[#6](-[#6])=[S+:1][#8-:2]>>[#6]-[#6](-[#6])=[S:1]=[O:2]"));
		standardizationReactions.add(new ReactionObject("SULFONE_STANDARDIZATION", "[#6][S;X3+]([#6])[#8-]", "[#6][S;X3+:1]([#6])[#8-:2]>>[#6][S:1]([#6])=[O:2]"));
		standardizationReactions.add(new ReactionObject("SULFONIUM_YLIDE_STANDARDIZATION", "[#6][S-]([#6])[#6+]", "[#6][S-:1]([#6])[#6+:2]>>;[#6][S:1]([#6])=[#6;A:2]"));
		standardizationReactions.add(new ReactionObject("SULFOXIDE_STANDARDIZATION", "[#6][S+]([#6])([#8-])=O", "[#6][S+:1]([#6])([#8-:2])=O>>[#6][S:1]([#6])(=[O:2])=O"));
		standardizationReactions.add(new ReactionObject("SULFOXONIUM_YLIDE_STANDARDIZATION", "[#6][S+]([#6])([#8-])=[#6;A]", "[#6][S+:1]([#6])([#8-:2])=[#6;A]>>[#6][S:1]([#6])(=[#6;A])=[O:2]"));
		standardizationReactions.add(new ReactionObject("TERTIARY_N_OXIDE_STANDARDIZATION", "[#6]-[#7;X4]=O", "[#6]-[#7;X4:1]=[O:2]>>[#6]-[#7+:1]-[#8-:2]"));
		standardizationReactions.add(new ReactionObject("THIOUREA_STANDARDIZATION", "[#7;X3]-[#6;X3](-[#16;H1X2])=[#7;X2]","[#7;X3:1]-[#6;X3:2](-[#16;H1X2:3])=[#7;X2:4]>>[#7;X3:1]-[#6;X3:2](-[#7;X3:4])=[S;X1:3]"));
		standardizationReactions.add(new ReactionObject("CARBAMIDOTHIOIC_ACID_TO_THIOUREA_STANDARDIZATION","[#7;X3]-[#6;X3](-[#16;H1X2])=[#7;X2]","[#7;X3:1]-[#6;X3:2](-[#16;H1X2:3])=[#7;X2:4]>>[#7;X3:1]-[#6;X3:2](-[#7;X3:4])=[S;X1:3]"));
		standardizationReactions.add(new ReactionObject("CARBOXYLIC_ACID_ANION_STANDARDIZATION", "[#8;X1-]-[#6;X3](-[#6,#1])=[O;X1]", "[#8;X1-:1]-[#6;X3:2](-[#6,#1:3])=[O;X1:4]>>[H][#8;X2:1]-[#6;X3:2](-[#6,#1:3])=[O;X1:4]"));

		
		//
	}
	
//	public static LinkedHashMap<String, ArrayList<String>>	setOfReactantSMARTS;
//	static {
//		setOfReactantSMARTS = new LinkedHashMap<String, ArrayList<String>>();
//
//		/***
//		 *  Standardization reactions
//		 ***/
//		setOfReactantSMARTS.put(ReactionName.CARBAMIDIC_ACID_TO_UREA_STANDARDIZATION.toString(),
//				new ArrayList<String>(Arrays.asList("[#7;X3]-[#6;X3](-[#8;H1X2])=[#7;X2]")));
//		setOfReactantSMARTS.put(ReactionName.SULFONIC_ACID_STANDARDIZATION.toString(),
//				new ArrayList<String>(Arrays.asList("[#8;X1-][S;X4]([#6,#1;A])(=[O;X1])=[O;X1]")));
//		setOfReactantSMARTS.put(ReactionName.ANTHOCYANIDIN_STANDARDIZATION_PATTERN1.toString(),
//				new ArrayList<String>(Arrays.asList("[#6;R1]=,:1[#6;R1]=,:[#6;R1][#6;R1](=,:[#6;R1][#6;R1]=,:1)-[#6;R1]1=,:[#6;R1][#6;R1]=,:[#6]2[#6]=,:[#6][#6]=,:[#6][#6]2=,:[#8+]1")));
//	}
//	
//	public static LinkedHashMap<String, String>			setOfSMIRKS;
//	static {
//		setOfSMIRKS = new LinkedHashMap<String, String>();
//
//		/**
//		 * 	Standardization reactions
//		 */
//
//		
//		setOfSMIRKS.put(ReactionName.CARBAMIDIC_ACID_TO_UREA_STANDARDIZATION.toString(),"[H][#8;X2:3]-[#6;X3:2](-[#7;X3:1])=[#7;X2:4]>>[H][#7;X3:4]-[#6;X3:2](-[#7;X3:1])=[O;X1:3]");
//		setOfSMIRKS.put(ReactionName.SULFONIC_ACID_STANDARDIZATION.toString(),"[#8;X1-:1][S;X4:2]([#6,#1;A:5])(=[O;X1:3])=[O;X1:4]>>[H][#8;X2:1][S;X4:2]([#6,#1;A:5])(=[O;X1:3])=[O;X1:4]");
////		setOfSMIRKS.put(ReactionName.COENZYME_A_STANDARDIZATION.toString(),"");
//
//		setOfSMIRKS.put(ReactionName.ANTHOCYANIDIN_STANDARDIZATION_PATTERN1.toString(),"[#6;R1:1]-1=[#6;R1:6]-[#6;R1:5]=[#6;R1:4](-[#6;R1:3]=[#6;R1:2]-1)-[#6;R1:7]1=[#6;R1:8]-[#6;R1:9]=[#6:10]-2-[#6:11]=[#6:12]-[#6:13]=[#6:14]-[#6:15]-2=[O+:16]1>>"
//				+ "[#6;R1:1]-1=[#6;R1:6]-[#6;R1:5]=[#6;R1:4](-[#6;R1:3]=[#6;R1:2]-1)-[#6;R1:7]-1=[O+:16][#6:15]-2=[#6:10](-[#6:11]=[#6:12]-[#6:13]=[#6:14]-2)-[#6;R1:9]=[#6;R1:8]-1");
//
//	}

}
