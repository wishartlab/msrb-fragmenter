package utils;

import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;

public class CorrectRAtoms {
	public static void reMapRAtoms(IAtomContainer mole_withR, IAtomContainer mole_origin) throws Exception {
		for(int i = 0; i < mole_withR.getAtomCount(); i++) {
			if(mole_withR.getAtom(i).getSymbol().equalsIgnoreCase("R") || mole_withR.getAtom(i).getSymbol().equalsIgnoreCase("A")) {
				Integer checkIdx = mole_withR.getAtom(i).getProperty("AtomIdx");
				for(int j = 0; j < mole_origin.getAtomCount(); j++) {
					if(mole_origin.getAtom(j).getProperty("AtomIdx") != null && mole_origin.getAtom(j).getProperty("AtomIdx") == checkIdx) {
						mole_withR.getAtom(i).setAtomicNumber(mole_origin.getAtom(j).getAtomicNumber());
						break;
					}
				}
			}
		}		
	}
	
	public static void assignAtomIdx(IAtomContainer molecule) throws Exception{
		for(int k = 0; k < molecule.getAtomCount(); k++) {
			IAtom oneAtom = molecule.getAtom(k);
			oneAtom.setProperty("AtomIdx", k);
		}
	}
}
