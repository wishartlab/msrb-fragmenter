# MSRB-Fragmenter batch scripts

These batch scripts are designed to run with the Slurm grid scheduler, and have been used on Compute Canada.


## Usage

Edit `msrb_fragmenter.sh` to set the appropriate account name, input file name, output file base name, etc.

Place `msrb_fragmenter.sh` and `msrb_fragmenter_batch.py` in an appropriate directory on
your cluster. Place `msrb-fragmenter.jar` in the same directory.

Make output directories:

    mkdir out_list results

The input file should consist on a tab-separated file with the InChIKey in the first column
and SMILES string in the second column. E.g.

    InChIKey=BRMWTNUJHUMWMS-LURJTMIESA-N	CN1C=NC(C[C@H](N)C(O)=O)=C1
    InChIKey=WHEUWNKSCXYKBU-QPWUGHHJSA-N	[H][C@@]12CCC(=O)[C@@]1(C)CC[C@]1([H])C3=C(CC[C@@]21[H])C=C(O)C(OC)=C3
    InChIKey=MXHRCPNRJAMMIM-SHYZEUOFSA-N	OC[C@H]1O[C@H](C[C@@H]1O)N1C=CC(=O)NC1=O

Submit your job:

    sbatch msrb_fragmenter.sh


## Output

The `results` directory will contain predicted spectra output files produced by MSRB-Fragmenter.

The `out_list` directory will contain tab-separated files containing InChIKey, SMILES, and
MSRB-Fragmenter status code. If MSRB-Fragmenter ran without crashing but did not produce a
status code (such as when it runs successfully), the status code column will contain '-'.
If MSRB-Fragmenter exited unexpectedly with an error, the operating system error code,
multiplied by -1, will appear in the status code column. You may combine the `out_list`
files together, e.g.

    cat out_list/myjob_* > myjob_out.tsv
