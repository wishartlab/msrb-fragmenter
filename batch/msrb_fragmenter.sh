#!/bin/bash
#SBATCH --time=00:10:00
#SBATCH --job-name=msrb_fragmenter
#SBATCH --account=rrg-dwishart
#SBATCH --output=msrb_fragmenter.out
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem=0
#SBATCH --array=1-50
echo 'Running MSRB-Fragmenter'
echo "Starting run at: `date`"

module load python/3.7
module load java/1.8

python msrb_fragmenter_batch.py -i input.tsv -o out_list/myjob -a $SLURM_ARRAY_TASK_ID -s 50

# ---------------------------------------------------------------------
echo "Job finished with exit code $? at: `date`"
# ---------------------------------------------------------------------
