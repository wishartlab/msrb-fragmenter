# MSRB-Fragmenter's README

"MSRB-Fragmenter" = Mass Spectrum Rule-Based Fragmenter

## Compiling MSRB-Fragmenter on the command line

Requires Java 8 (or preferrably, Java 11). Tested with Maven 3.6.3.

1. Validate, compile, test, and package the the compiled code into a `.jar` file, saved into the `target` directory:

    ```mvn package```

2. Copy the built `.jar` file into the `bin` directory:

    ```mv target/msrb-fragmenter-* bin/```

3. Clean up artifacts created by prior builds:

    ```mvn clean```


## Compiling MSRB-Fragmenter with Eclipse

This tutorial assumes that you have Eclipse installed, as well as the Maven Development Tools (m2e 1.6+) 0.30. If you haven't please select the section "Setting up Eclipse and Maven". The current version of the msrb-fragemnter requires Java 8.

1. Cloning the project repository directly from Eclipse
	- Click on import projects
	- Select Git
	- Fill out the form: Usually, adding the https URI will prompt an automated filling of the other fields (repository path, host name, username)
	- add the user password
	- Click on ''
	- In the Destination menu, point to the correct destination
	- Select the correct branch you want
	- Then select 'Next'
	- In the window 'Select a wizard to use for importing projects', select 'Import existing Eclipse projects' and click 'Next'
	- In the "Import Projects" window, select the correct project, and click 'Finish'

2. Import the repository From an existing local project, if is has been cloned locally
	- Select Import projects -> General -> Existing Projects into Workspace -> Select root directory

3. If you get an error message for missing src folders, e.g. "The project cfmid is missing source folder '/src/main/resources' "
	- Right-Click on the Project Name
	- Select Build Path -> Configure Build Path
	- In the appearing window, click on the 'source' button:
		- Select each of the missing folders, and click on the 'Remove' button on the right.
		- Select 'Apply'
		- On the right menu: click "Add Folder" -> "Create Folder"
		- Add the path of the missing folder (e.g. /src/main/resources) under 'Folder name:'
		- Click 'Finish'
		- Repeat this for any missing folder

4. Selecting the correct Java version for the project
	- Go to the Build Path -> Configure Build Path
	- Click on 'Libraries', then select the current version of the JRE to be removed, and click on the "Remove" button.
	- Then select "Add Library" -> "JRE System Library"
	- On the new window, select the correct JRE version

5. Include Maven libraries in built JAR files
	- Go to the Build Path -> Configure Build Path
	- Click on 'Order and Export', and select the checkbox next to 'Maven Dependencies', and click 'Apply and Close'.

6. Creating an executable
	- Right-click on the project's name and select "export"
	- In the newly opened window, select Java -> Runnable JAR file
	- Select Next
	- In the "Runnable JAR File export" window:
		- Select "RuleBasedFrag" for the launch configuration
		- Select "cfmid_plus/bin/msrb-fragmenter.jar" as Export destination
		- Make sure to select "Extract required libraries into generated JAR" in the Library handling section
		- Check the box "Save as ANT script", and add the ANT script location
		- Click Finish


## Running MSRB-Fragmenter

To see usage instructions, run

      java -jar msrb-fragmenter.jar --help

Example 1 - Predict spectra for input SMILES, for all adduct types:

      java -jar msrb-fragmenter.jar -ismi 'CCCCCCCCCCCCCCCC(=O)OCC(COP(O)(=O)OC[C@H](N)C(O)=O)OC(=O)CCCCCCCCCCCCCCC' -o output.txt

Example 2 - Predict spectrum for input SMILES, for a single adduct:

      java -jar msrb-fragmenter.jar -a [M+H]+ -ismi 'CCCCCCCCCCCCCCCC(=O)OCC(COP(O)(=O)OC[C@H](N)C(O)=O)OC(=O)CCCCCCCCCCCCCCC' -o output.txt

Example 3 - Predict spectra for input SMILES, for selected adduct types:

      java -jar msrb-fragmenter.jar -a '[M+H]+;[M-H]-' -ismi 'CCCCCCCCCCCCCCCC(=O)OCC(COP(O)(=O)OC[C@H](N)C(O)=O)OC(=O)CCCCCCCCCCCCCCC' -o output.txt

Note: Adduct names will be added to the output filenames.


## Status Reports

MSRB-Fragmenter outputs status report codes based on the class of the input compounds and
the adduct arguments provided.

- "STATUS REPORT = 0": No adduct was specified and the compound belongs to a chemical
  class covered by MSRB-Fragmenter. MSRB-Fragmenter will predict spectra for all of the
  adduct types it can.
- "STATUS REPORT = 1": Each specified adduct is covered for the chemical class of the
  compound, and corresponding spectra will be predicted.
- "STATUS REPORT = 2": Some adducts are not covered for the chemical class of the query
  compound, and spectra will be computed for the remaining adducts. This applies if you
  provide more than one adduct.
- "STATUS REPORT = 3": The only adduct specified by the user is not covered for the
  chemical class of the query compound, and spectra will be computed for all adducts
  applicable for the query compound. This applies if you provide only one adduct.
- "STATUS REPORT = 4": Invalid chemical class. The query compound belongs to a lipid
  class not covered in the current version of the fragmenter. CFM-ID's combinatorial
  fragmentation method is also expected to perform poorly on the input compound.
- "STATUS REPORT = 5": The query compound does not belong to any of the classes covered in
  the current version of the fragmenter; nor does it belong to the classes of
  glycerolipids, glycerophpspholipids, sphingolipids, ether lipids. A better choice would
  be to use CFM-ID's combinatorial fragmentation method.

## Supported Adducts ##
### Note, adducts support may vary based on input ###

* [M+H]+
* [M-H]-
* [M+]
* [M+Li]+
* [M+NH4]+
* [M+Na]+
* [M+Na]+
* [M+Cl]-
* [M-2H]\(2H\)-

## Supported Chemical Classes ##
Supported chemical classes are classified by Classyfire Toolkit <http://classyfire.wishartlab.com/>
###  Version 1.0.8 ###
* 1-Monoacylglycerols
* 2-Monoacylglycerols
* 1,2-Diacylglycerols
* Triacylglycerols
* Phosphatidic acids
* Phosphatidylcholines
* Phosphatidylethanolamines
* Lysophosphatidylcholines
* Lysophosphatidic acids
* Phosphatidylserines
* Ceramides
* Sphingomyelins
* Cardiolipins
* Phosphatidylglycerols
* Lysophosphatidylglycerols
* 1-alkyl,2-acylglycero-3-phosphocholines
* 1-(1Z-alkenyl)-glycero-3-phosphocholines
* Monoalkylglycerophosphocholines
* 1-(1Z-alkenyl)-glycero-3-phosphocholines
* Phosphatidylinositols
* Lysophosphatidylinositols

### Additional Classes Supported by Version 1.1.0 ###
* acylcarnitines 
* acylcholines 
* flavonols 
* flavones 
* flavanones 
* flavonoid-3-O-glycosides 
* flavonoid-7-O-glycosides 
* flavonoid-7-O-glucuronides 
* 4'-O-methylated flavonoids 
* 7-O-methylated flavonoids 
* 3'-O-methylated flavonoids 